**React Hangman Game**

A classic hangman game built with React and hooks. The game involves guessing a word by selecting letters while avoiding incorrect guesses. Below are the key features and steps involved in creating this interactive hangman experience:

**Application Preview screenshot**:

![hangman-react](./images/hangman-react.png)



**Key Features**

1. Component Structure: Organized into components for Header, Figure, Wrong Letters, Word, Popup, and Notification.
2. State Management: Utilizes useState and useEffect hooks for efficient state management.
3. Word Display: Generates and displays a random word with correct letters filled in.
4. Wrong Letters Update: Tracks and displays incorrect letters chosen by the player.
5. Hangman Figure: Renders the hangman figure dynamically using SVG.
6. Notifications: Alerts players for repeated letter selections.
7. Popup on Win/Lose: Displays popups for win or lose scenarios.
8. Play Again Button: Allows users to reset and play the game again.


**Getting Started**

- Clone the repository

- Install dependencies: npm install

- Start the React development server: npm start


**How to Play**

- Begin the game by typing and guessing letters to uncover the word.

- View the hangman figure and wrong letters as the game progresses.

- Receive notifications for repeated letter selections or win/lose scenarios.

- Play again by clicking the "Play Again" button to reset the game.

Feel free to explore the codebase, customize the game, and enhance features as desired. This project provides a fun and interactive hangman experience using React and hooks. Enjoy the game!
